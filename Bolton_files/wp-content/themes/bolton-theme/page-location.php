<?php
/*
	Template Name: Page location
*/
?>

<?php get_header('pages'); ?>


<?php get_template_part('templates/location', 'header'); ?>


    <section class="quote-section quote-green">
        <div class="quote-inner-content">
            <div class="pos-center">
                <blockquote class="quote">
                    <p>
                        <cite>
                            "Bolton is a great place to do business with a good pool of labour and local
                            stakeholders who are willing to work with you to make your business move to the
                            area a success."
                        </cite>
                    </p>

                    <footer class="footer-quote">
                        <span class="auothor-quote">Gareth Hughes, director of procurement, Whistl</span>
                    </footer>
                </blockquote>
            </div>
        </div>
    </section>

    <section id="location-section">

        <div class="location-section-inner">
            <div class="location-content-inner">
                <div class="location-box">
                    <ul class="key-facts facts-right">
                        <li><span class="inner-facts">12 miles from</span><span
                                    class="inner-facts">Manchester city centre</span></li>
                        <li><span class="inner-facts">20 miles from</span><span
                                    class="inner-facts">Manchester Airport</span></li>
                        <li><span class="inner-facts">60% of UK businesses</span><span
                                    class="inner-facts">within a 2-hour drive</span></li>
                        <li><span class="inner-facts"></span>1 million people live <span
                                    class="inner-facts">within 45 minutes</span></li>
                    </ul>
                </div>

                <div class="location-box">
                    <ul class="key-facts facts-left">
                        <li><span class="inner-facts">12 minute train journey</span><span
                                    class="inner-facts">to Manchester</span></li>
                        <li><span class="inner-facts">Direct access to the</span><span
                                    class="inner-facts">M61, M60, M62 and M6</span></li>
                        <li><span class="inner-facts">Second highest employee</span><span
                                    class="inner-facts">base in Greater Manchester</span></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="map-content-location">
            <div class="location-parent-wrapper">
                <?php get_template_part('templates/location', 'animation'); ?>
            </div>
        </div>


    </section>


<?php get_footer('pages'); ?>