<?php
/**
 * Template Name: Page Contact Us
 */

the_post();
get_header('pages');
?>

<main class="page-contact-us">
    <?php get_template_part('templates/header', 'hero') ?>

    <section class="global-section">
        <div class="content-center narrow-content">
            <?php the_content() ?>

            <?= do_shortcode('[contact-form-7 id="4" title="Contact form"]') ?>
        </div>
    </section>
</main>

<?php
get_footer('bar');