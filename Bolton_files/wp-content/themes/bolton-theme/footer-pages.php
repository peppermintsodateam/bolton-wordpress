    <footer id="main-page-footer">        
        <a href="<?php the_field('main_logo_url', 'options'); ?>" id="footer-logo"><img src="/wp-content/uploads/2018/06/bolton-council-logo-horizontal.png" alt="Bolton Council" width="307" height="32" /></a>        
        <ul class="social-nav-top">
            <?php if (have_rows('page_header_social_media', 'options')) :
                while (have_rows('page_header_social_media', 'options')) :
                    the_row(); ?>
                    <li><a target="_blank" href="<?= get_sub_field('social_url'); ?>"><i class="<?= get_sub_field('social_class'); ?>"></i></a></li>
                <?php endwhile;
            endif; ?>
        </ul>
        <p class="copyrights"><a href="<?php the_field('enquire_btn_url', 'options'); ?>">Contact us</a>|<a href="http://www.bolton.gov.uk/home/Pages/Copyrightdisclaimerandprivacy.aspx">Privacy policy</a></p>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>