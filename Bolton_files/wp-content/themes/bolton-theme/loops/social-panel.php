<ul class="social-nav-bottom">
    <?php if (have_rows('social_list', 'options')) :
        while (have_rows('social_list', 'options')) :
            the_row(); ?>

            <li>
                <a target="_blank" href="<?= get_sub_field('link'); ?>">
                    <i class="<?= get_sub_field('class'); ?>"></i>
                </a>
            </li>
        <?php endwhile;
    endif; ?>
</ul>


  