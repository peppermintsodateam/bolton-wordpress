<?php
$socialGroups = [
    'facebook' => [
        'icon' => 'fab fa-facebook-f',
        'linkPrefix' => 'http://facebook.com/',
    ],
    'twitter' => [
        'icon' => 'fab fa-twitter',
        'linkPrefix' => 'http://twitter.com/',
    ],
    'linkedin' => [
        'icon' => 'fab fa-linkedin-in',
        'linkPrefix' => 'http://linkedin.com/company/',
    ],
];

if (have_posts()) :
    while (have_posts()) :
        the_post(); ?>
        <div class="slide">
            <section class="global-section">
                <div class="content-center narrow-content">
                    <figure class="main-logo-top">
                        <img alt="<?php the_title() ?>"
                             src="<?= wp_get_attachment_image_url(get_post_thumbnail_id(), 'full'); ?>"
                        />
                    </figure>

                    <div class="slide-inner">
                        <?php if (get_field('show_company_name')) : ?>
                            <h3><?php the_title() ?></h3>
                        <?php endif ?>

                        <?php the_content() ?>

                        <?php if ($url = get_field('link_url')) : ?>
                            <a href="<?= $url ?>" target="_blank">Link to website</a>
                        <?php endif ?>

                        <ul class="social-nav">
                            <?php foreach ($socialGroups as $socialGroup => $social) :
                                if ($link = get_field('social_' . $socialGroup)) : ?>
                                    <li>
                                        <a target="_blank" href="<?= $social['linkPrefix'] . $link ?>">
                                            <i class="<?= $social['icon']?>"></i>
                                        </a>
                                    </li>
                                <?php endif;
                            endforeach ?>
                        </ul>
                    </div>
                </div>
            </section>
        </div>
    <?php endwhile;
endif; ?>
