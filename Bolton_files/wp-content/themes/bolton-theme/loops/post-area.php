<?php
$i = 1;
$postType;

switch (true) {
    case (is_page(104)) :
        $postType = 'crompton_place';
        break;

    case (is_page(109)) :
        $postType = 'cheadle_square';
        break;

    case (is_page(139)) :
        $postType = 'trinity_quarter';
        break;

    case (is_page(145)) :
        $postType = 'croal_valley';
        break;

    case (is_page(151)) :
        $postType = 'church_wharf';
        break;

    case (is_page(158)) :
        $postType = 'wider_regen';
        break;
}

$args = [
    'post_type' => $postType,
    'post_status' => 'publish',
    'posts_per_page' => -1,
];

$rotator_loop = new WP_Query($args);

if ($rotator_loop->have_posts()) :
    while ($rotator_loop->have_posts()) :
        $rotator_loop->the_post();
        //variables
        $choose_layout = get_field('choose_layout');
        $choose_media = get_field('choose_media_type');
        $choose_icon_type = get_field('choose_icon_type');
        $choose_youtube = get_field('youtube_link');
        $youtube_poster = get_field('youtube_poster');
        $youtube_poster_url = get_field('youtube_poster_url');
        $youtube_alt = get_field('youtube_alt');
        $youtube_caption = get_field('youtube_caption');
        $image_icon = get_field('image_icon');
        $image_icon_alt = get_field('image_icon_alt');
        $quote_text = get_field('quote_text');
        ?>

        <div id="<?php the_slug(); ?>" class="slide">
            <section class="main-slide-wrapper">
                <div class="top-logo-wrapper">
                    <figure class="main-logo-top">
                        <img alt="<?= get_post_type_object($postType)->labels->singular_name ?>"
                             src="<?= do_shortcode('[fx-get-thumbnail]'); ?>"/>
                    </figure>
                </div>

                <div class="slide-inner">
                    <?php if ($choose_layout == 'text'): ?>
                        <div class="col-left col-flex half-flex">
                            <?php if ($i == 1) : ?>
                                <h1 class="slide-header"><?php the_title(); ?></h1>
                            <?php else : ?>
                                <h2 class="slide-header"><?php the_title(); ?></h2>
                            <?php endif;

                            $i++;

                            if (get_field('show_quote_footer')): ?>
                                <div class="text-wrapper">
                                    <blockquote class="text-quote">
                                        <p><?= $quote_text; ?></p>

                                        <footer class="quote-footer">
                                            <cite>
                                                <?php if (have_rows('author_details')) :
                                                    while (have_rows('author_details')) : the_row(); ?>
                                                        <div class="author-details">
                                                            <span class="testimonial-details"><?php the_sub_field('authors_name'); ?></span>
                                                            <span class="testimonial-details"><?php the_sub_field('authors_business'); ?></span>
                                                        </div>
                                                    <?php endwhile;
                                                endif; ?>
                                            </cite>
                                        </footer>
                                    </blockquote>
                                </div>
                            <?php else: ?>
                                <div class="text-wrapper">
                                    <?php the_content(); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($choose_layout == 'list'): ?>

                        <div class="col-left col-flex wider-flex">
                            <h2 class="slide-header"><?php the_title(); ?></h2>

                            <div class="text-wrapper">
                                <?php if (have_rows('items_list')) : ?>
                                    <?php while (have_rows('items_list')) : the_row(); ?>
                                        <?php
                                        //variables
                                        $icon = get_sub_field('icon');
                                        $icon_alt = get_sub_field('icon_alt');
                                        $list_text = get_sub_field('list_text');
                                        ?>

                                        <div class="list-row">
                                            <figure class="icon-wrapper">
                                                <img src="<?= $icon; ?>"
                                                     alt="<?= $icon_alt ?>">
                                            </figure>

                                            <div class="row-text">
                                                <p><?= $list_text; ?></p>
                                            </div>
                                        </div>

                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                        </div>

                    <?php endif; ?>


                    <?php if ($choose_layout == 'text') : ?>

                        <div class="col-right col-flex half-flex">

                            <?php if ($choose_media == 'youtube') : ?>
                                <div class="pop-up-wrapper">
                                    <a href="<?= $choose_youtube; ?>"
                                       class="img-container popup-vimeo">
                                        <div class="you-tube-mask"></div>
                                        <img src="<?= $youtube_poster ? $youtube_poster : $youtube_poster_url; ?>"
                                             alt="<?= $youtube_alt ?>">
                                    </a>
                                    <span class="watch-video"><?= $youtube_caption; ?></span>
                                </div>
                            <?php endif; ?>

                            <?php if ($choose_media == 'image') : ?>


                                <?php if ($choose_icon_type == 'photo') : ?>
                                    <a href="<?= $image_icon; ?>"
                                       class="icon-wrapper-img popup-image">
                                        <img class="img-border" src="<?= $image_icon; ?>"
                                             alt="<?= $image_icon_alt; ?>">
                                    </a>
                                <?php endif; ?>

                                <?php if ($choose_icon_type == 'map') : ?>
                                    <figure class="icon-wrapper-img map-icon">
                                        <img src="<?= $image_icon; ?>"
                                             alt="<?= $image_icon; ?>">
                                    </figure>
                                <?php endif; ?>

                            <?php endif; ?>

                            <?php if ($choose_media == 'youtube-thumbnail') : ?>

                                <a href="<?= $youtube_poster; ?>"
                                   class="icon-wrapper-img popup-image">
                                    <img class="img-border" src="<?= $youtube_poster; ?>"
                                         alt="<?= $youtube_alt ?>">
                                </a>


                                <div class="video-horizontal-wrapper">

                                    <?php if (have_rows('youtube_thumbnails')) : ?>

                                        <?php while (have_rows('youtube_thumbnails')) : the_row(); ?>

                                            <?php
                                            //variables
                                            $youtube_thumb_link = get_sub_field('youtube_thumb_link');
                                            $youtube_thumb_poster = get_sub_field('youtube_thumb_poster');
                                            $youtube_thumb_poster_url = get_sub_field('youtube_thumb_poster_url');
                                            $youtube_thumb_alt = get_sub_field('youtube_thumb_alt');
                                            $youtube_caption = get_sub_field('youtube_caption');
                                            ?>

                                            <div class="img-wrapper">
                                                <a href="<?= $youtube_thumb_link; ?>"
                                                   class="img-container popup-youtube">
                                                    <div class="you-tube-mask"></div>
                                                    <img src="<?= $youtube_thumb_poster ? $youtube_thumb_poster : $youtube_thumb_poster_url; ?>"
                                                         alt="<?= $youtube_thumb_alt ?>">

                                                </a>
                                                <span class="watch-video"><?= $youtube_caption; ?></span>
                                            </div>

                                        <?php endwhile; ?>

                                    <?php endif; ?>
                                </div>

                            <?php endif; ?>

                        </div>

                    <?php endif; ?>

                    <?php if ($choose_layout == 'list') : ?>

                        <div class="col-right col-flex tight-flex pop-up-wrapper">

                            <?php if ($choose_media == 'image') : ?>


                                <?php if ($choose_icon_type == 'photo') : ?>
                                    <a href="<?= $image_icon; ?>"
                                       class="icon-wrapper-img popup-image">
                                        <img class="img-border" src="<?= $image_icon; ?>"
                                             alt="<?= $image_icon_alt; ?>">
                                    </a>
                                <?php endif; ?>

                                <?php if ($choose_icon_type == 'map') : ?>
                                    <figure class="icon-wrapper-img map-icon">
                                        <img src="<?= $image_icon; ?>"
                                             alt="<?= $image_icon; ?>">
                                    </figure>
                                <?php endif; ?>

                            <?php endif; ?>

                            <?php if ($choose_media == 'youtube') : ?>
                                <div class="pop-up-wrapper">
                                    <a href="<?= $choose_youtube; ?>"
                                       class="img-container popup-vimeo">
                                        <div class="you-tube-mask"></div>
                                        <img src="<?= $youtube_poster; ?>"
                                             alt="<?= $youtube_alt ?>">
                                    </a>
                                    <span class="watch-video"><?= $youtube_caption; ?></span>
                                </div>
                            <?php endif; ?>

                        </div>

                    <?php endif; ?>
                </div>

            </section>
        </div>

    <?php endwhile; ?>

    <?php wp_reset_postdata(); ?>

<?php else: ?>

    <p><?php _e('Sorry, no intervention areas matched your criteria.'); ?></p>


<?php endif; ?>