<?php
/*
Template Name: Page home
 */
?>

<?php get_header('pages');?>
<section id="home-banner-section" class="home-banner-section">
    <div class="content-center center">
        <p class="top-title-slogan">
            <?=get_field('top_slogan')?>
        </p>
        <?php get_template_part('templates/logo', 'home');?>
        <div class="stripe-wrapper">
            <span class="col-line"></span>
            <span class="col-line"></span>
            <span class="col-line"></span>
            <span class="col-line"></span>
            <span class="col-line"></span>
            <span class="col-line"></span>
            <span class="col-line"></span>
            <span class="col-line"></span>
            <span class="col-line"></span>
            <span class="col-line"></span>
            <span class="col-line"></span>
            <span class="col-line"></span>
            <span class="col-line"></span>
            <span class="col-line"></span>
            <span class="col-line"></span>
        </div>
        <div class="services-home-list">

            <?php
if (have_rows('list_of_services')):
    while (have_rows('list_of_services')): the_row();
        $service = get_sub_field('service');
        ?>
        <span class="serv-el"><?=$service;?></span>
    <?php endwhile;
endif;?>
        </div>
        <div class="bottom-slogan-text">
            <?php get_template_part('loops/loop', 'home');?>
        </div>
    </div>
</section>
<section id="home-developments-section" class="home-developments-section">
    <div id="development-left" class="development-block">
        <div class="center">
            <h2>Town Centre<br />Developments</h2>
            <a href="<?=do_shortcode('[fx-link id="20"]');?>">Explore developments</a>
        </div>
    </div>
    <div id="development-right" class="development-block">
        <div class="center">
            <h2>Wider Area<br />Developments</h2>
            <a href="<?=do_shortcode('[fx-link id="412"]');?>">Find our more</a>
        </div>
    </div>
</section>
<section class="quote-section quote-blue">
    <div class="quote-inner-content">
        <div class="pos-center">
            <blockquote class="quote">
                <p>
                    <cite>
                        <?php the_field("cite"); ?>
                    </cite>
                </p>
                <footer class="footer-quote">
                    <span class="auothor-quote"><?php the_field('cite_footer'); ?></span>
                </footer>
            </blockquote>
        </div>
    </div>
</section>
<section id="home-services-section" class="home-services-section">
    <?php
    $service_rows = get_field('list_of_service_descriptions');
    //$first_row_image = $first_row['sub_field_name' ]; // get the sub field value
    ?>
    <div id="services-left" class="services-block">
        <a href="/sponsors-partners/">
        <div class="center">
            <span class="image-icon-wrapper">
                <img src="/wp-content/uploads/2018/06/home-icon-handshake.png" alt="Sponsors-Partners" />
            </span>
            <h3>Sponsors &amp; Partners</h3>
            <p><?=$service_rows[0]['description_text'];?></p>
        </div>
            </a>
    </div>
    <div id="services-mid" class="services-block">
        <a href="/why-invest/">
        <div class="center">
            <span class="image-icon-wrapper">
                 <img src="/wp-content/uploads/2018/06/home-icon-speech.png" alt="Why Invest"/>
            </span>
           
            <h3>Why Invest</h3>
            <p><?=$service_rows[1]['description_text'];?></p>
        </div>
             </a>
    </div>
    <div id="services-right" class="services-block">
         <a href="/about-bolton/">
        <div class="center">
            <span class="image-icon-wrapper">
                <img src="/wp-content/uploads/2018/06/home-icon-calendar.png" alt="about-boloton"  />
            </span>
            
            <h3>About Bolton</h3>
            <p><?=$service_rows[2]['description_text'];?></p>
        </div>
             </a>
    </div>
</section>
<section id="home-map-section" class="home-map-section">
    <nav id="map-menu"></nav>
    <div id='map' style='width: 100%; height: 100%;'></div>
    <script>var selected_map = 'home';</script>
</section>
<?php get_footer('pages'); ?>