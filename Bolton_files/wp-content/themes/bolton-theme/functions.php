<?php
/*--------------------------------
        THEME CONFIGURATION
--------------------------------*/
define('THEME_PATH', get_template_directory_uri());

/*--------------------------------
   DEFINE PATH (DIFFERENT METHOD)
--------------------------------*/

if (!defined('BOLTON_THEME_DIR')) {
    define('BOLTON_THEME_DIR', get_theme_root().'/'.get_template().'/');
}

require_once BOLTON_THEME_DIR.'libs/posttypes.php';

/*--------------------------------
        LOAD JQUERY
--------------------------------*/

function jquery_scripts_method()
{
    wp_enqueue_script('jquery');
}

add_action('wp_enqueue_scripts', 'jquery_scripts_method');


// Defer Javascripts
// Defer jQuery Parsing using the HTML5 defer property
if (!(is_admin())) {
    function defer_parsing_of_js($url)
    {
        if (false === strpos($url, '.js')) {
            return $url;
        }
        if (strpos($url, 'jquery.js')) {
            return $url;
        }

        // return "$url' defer ";
        return "$url' defer onload='";
    }

    add_filter('clean_url', 'defer_parsing_of_js', 11, 1);
}


/*--------------------------------
    CUSTOM JQUERY PLUGINS
--------------------------------*/

function files_register()
{
    wp_register_style('fonts', 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900" rel="stylesheet', [], null, 'all');
    wp_register_style('mapbox_css', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.45.0/mapbox-gl.css', [], null, 'all');
    wp_register_style('select2_css', sprintf('%s/css/select2.min.css', get_template_directory_uri()), [], null, 'all');
    wp_register_style('css', sprintf('%s/css/main%s.css', get_template_directory_uri(), (WP_DEBUG ? '' : '.min')), [], '1.0', 'all');

    wp_enqueue_style('fonts');
    wp_enqueue_style('mapbox_css');
    wp_enqueue_style('select2_css');
    wp_enqueue_style('css');

    $vendorPath = get_template_directory_uri().'/js/vendor';

    wp_register_script('font_awesome', 'https://use.fontawesome.com/releases/v5.0.6/js/all.js', [], null, 'all');
    wp_register_script('mapbox_js', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.45.0/mapbox-gl.js', [], null, 'all');
    wp_register_script('modernizr', $vendorPath . '/modernizr.min.js', ['jquery'], '2.8.3', false);
    wp_register_script('drawSVG', $vendorPath . '/DrawSVGPlugin.min.js', ['jquery'], '1.18.0', true);
    wp_register_script( 'map_api', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBNl9HP9_iCtVCNEE3x3Sf5zbb40vUXWtE', array('jquery'),"",  false);
    wp_register_script('mapsAll', $vendorPath . '/maps-home.js', ['jquery'], '1.18.0', true);
    wp_register_script('mapChurch', $vendorPath . '/map-church.js', ['jquery'], '1.18.0', true);
    wp_register_script('mapCheadle', $vendorPath . '/map-cheadle.js', ['jquery'], '1.18.0', true);
    wp_register_script('mapCroal', $vendorPath . '/map-croal.js', ['jquery'], '1.18.0', true);
    wp_register_script('mapCrompton', $vendorPath . '/map-crompton.js', ['jquery'], '1.18.0', true);
    wp_register_script('mapTrinity', $vendorPath . '/map-trinity.js', ['jquery'], '1.18.0', true);
    wp_register_script('owl', $vendorPath . '/owl.carousel.min.js', ['jquery'], '1.18.0', true);
    wp_register_script('select2', $vendorPath . '/select2.min.js', ['jquery'], '4.0.6-rc.1', true);
    wp_register_script('scripts_extra', sprintf('%s/js/scripts%s.js', get_template_directory_uri(), (WP_DEBUG ? '' : '')), ['jquery'], "1.0", true);
    wp_register_script('scripts', sprintf('%s/js/main%s.js', get_template_directory_uri(), (WP_DEBUG ? '' : '.min')), ['jquery'], "1.0", true);


     

    //  if(is_page(322)) {
    //     wp_enqueue_script('map_api');
    //     wp_deregister_script( 'mapbox_js');
    //     wp_enqueue_script('mapCheadle');
    // };
    

   
    wp_enqueue_script('modernizr');
    wp_enqueue_script('font_awesome');
    wp_enqueue_script('drawSVG');
    wp_enqueue_script('mapbox_js');
    wp_enqueue_script('select2');

       if ( is_archive('sponsors') ) {
        wp_enqueue_script('owl');
        wp_enqueue_script('scripts_extra');
       };


       if ( is_front_page() && is_home() ) {
            wp_enqueue_script('mapsAll');
       }elseif(is_page(5)) {
         wp_enqueue_script('mapsAll');
       }

        if(is_page(357)) {
            //wp_enqueue_script('map_api');
            //wp_deregister_script( 'mapbox_js');
            wp_enqueue_script('mapChurch');
        };


          if(is_page(350)) {
            wp_enqueue_script('mapCroal');
        };


        if(is_page(339)) {
            wp_enqueue_script('mapCrompton');
        };


      if(is_page(322)) {
        wp_enqueue_script('mapCheadle');
    };


     if(is_page(345)) {
        wp_enqueue_script('mapTrinity');
    };




    wp_enqueue_script('scripts');
}// end of function

add_action('wp_enqueue_scripts', 'files_register');


/* Remove Emoji Styles & Scripts*/
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');


/*--------------------------------
 FEATURED IMAGES IN DASHBOARD SUPPORT
--------------------------------*/

the_post_thumbnail(); // Without parameter ->; Thumbnail
the_post_thumbnail('thumbnail'); // Thumbnail (default 150px x 150px max)
the_post_thumbnail('medium'); // Medium resolution (default 300px x 300px max)
the_post_thumbnail('medium_large'); // Medium-large resolution (default 768px x no height limit max)
the_post_thumbnail('large'); // Large resolution (default 1024px x 1024px max)
the_post_thumbnail('full'); // Original image resolution (unmodified)
the_post_thumbnail([100, 100]); // Other resolutions (height, width)

if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(150, 150,
        true); // default Featured Image dimensions (cropped)

    add_image_size('custom-size', 9999, 9999);
}


function the_title_excerpt(
    $before = '',
    $after = '',
    $echo = true,
    $length = false
) {
    $title = get_the_title();

    if ($length && is_numeric($length)) {
        $title = substr($title, 0, $length);
    }

    if (strlen($title) > 0) {
        $title = apply_filters('the_title_excerpt', $before.$title.$after,
            $before, $after);
        if ($echo) {
            echo $title;
        } else {
            return $title;
        }
    }
}


// [fx-get-thumbnail]
function fx_get_post_thumbnail()
{

    $thumbnail_id = get_post_thumbnail_id();

    $thumbnail_url = wp_get_attachment_url($thumbnail_id);

    return $thumbnail_url;

}

add_shortcode("fx-get-thumbnail", "fx_get_post_thumbnail");

/*--------------------------------
        REMOVE ADMIN BAR
--------------------------------*/
add_filter('show_admin_bar', '__return_false');

/*--------------------------------
        <p> AND <br> TAGS SPACES
--------------------------------*/
function remove_empty_p($content)
{
    $content = force_balance_tags($content);

    return preg_replace('#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content);
}

add_filter('the_content', 'remove_empty_p', 20, 1);


remove_filter('the_sidebar', 'wpautop');
remove_filter('the_excerpt', 'wpautop');
remove_filter('acf_the_content', 'wpautop');

/*--------------------------------
 NAVIGATION MAIN
--------------------------------*/
add_action('init', 'bolton_menu');

function bolton_menu()
{
    register_nav_menus(
        [
            'left-menu'  => __('Main Menu'),
            'right-menu' => __('Secondary menu'),
        ]
    );
}

/*--------------------------------
 LINK PAGE
--------------------------------*/
function fx_get_page_link($id_page)
{

    $id_page = intval($id_page['id']);

    $link = get_permalink($id_page);

    return $link;
}

add_shortcode("fx-link", "fx_get_page_link");

/*Using ACF page options*/
if (function_exists('acf_add_options_page')) {


    acf_add_options_page([
        'page_title' => 'Nav panel settings',
        'menu_title' => 'Nav panel settings',
        'menu_slug'  => 'nav-panel-settings',
        'capability' => 'edit_posts',
        'redirect'   => false,
    ]);


    acf_add_options_page([
        'page_title' => 'Slider Header Settings',
        'menu_title' => 'Slider Header Settings',
        'menu_slug'  => 'slider-header-settings',
        'capability' => 'edit_posts',
        'redirect'   => false,
    ]);


    acf_add_options_page([
        'page_title' => 'Page Header Settings',
        'menu_title' => 'Page Header Settings',
        'menu_slug'  => 'page-header-settings',
        'capability' => 'edit_posts',
        'redirect'   => false,
    ]);


}


/*--------------------------------
 FETCH THE SLUG FUNCTION
--------------------------------*/

function the_slug($echo = true)
{

    $slug = basename(get_permalink());

    do_action('before_slug', $slug);
    $slug = apply_filters('slug_filter', $slug);
    if ($echo) {
        echo $slug;
    }
    do_action('after_slug', $slug);

    return $slug;
}

/*--------------------------------
 EXCERPT FUNCTION
--------------------------------*/

function the_excerpt_max_charlength($charlength)
{
    echo cutText(get_the_excerpt(), $charlength);
}


function cutText($text, $maxLength)
{

    $maxLength++;

    $return = '';
    if (mb_strlen($text) > $maxLength) {
        $subex = mb_substr($text, 0, $maxLength - 5);
        $exwords = explode(' ', $subex);
        $excut = -(mb_strlen($exwords[count($exwords) - 1]));
        if ($excut < 0) {
            $return = mb_substr($subex, 0, $excut);
        } else {
            $return = $subex;
        }
        $return .= '[...]';
    } else {
        $return = $text;
    }

    return $return;
}

?>