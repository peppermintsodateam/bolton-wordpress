<?php
/*
  Template Name: Intervention page
*/

get_header('pages'); ?>
<main>
    <section id="wrapper-sections">
        <?php
        $args = [
            'post_type' => 'intervention_areas',
            'post_status' => 'publish',
            'posts_per_page' => 6,
        ];

        $area_loop = new WP_Query($args);

        if ($area_loop->have_posts()) :
            while ($area_loop->have_posts()) :
                $area_loop->the_post();
                //variables
                $btn_colors = get_field('choose_button_color');
                ?>
                <article class="service-sections">
                    <div class="mask-services"></div>

                    <div class="logo-wrapper">
                        <div class="logo-container">
                            <?php
                            $title_attribute = the_title_attribute(['echo' => false]);
                            if (has_post_thumbnail()) : ?>
                                <img src="<?php the_post_thumbnail_url('full') ?>"
                                    class="img-responsive"
                                    alt="<?= $title_attribute ?>"
                                    title="<?= $title_attribute ?>"
                                 />
                            <?php endif ?>
                        </div>
                    </div>

                    <div class="main-content-wrapper">
                        <div class="content-inner">
                            <?php if (have_rows('intervention_content')) : ?>
                                <section class="map-top-section">
                                    <?php while (have_rows('intervention_content')) :
                                        the_row();
                                        //variables
                                        $img_class = get_sub_field('img_class');
                                        $img_icon = get_sub_field('img_icon');
                                        $img_alt = get_sub_field('img_alt');
                                        ?>
                                        <div class="<?= $img_class; ?> inner-icons">
                                            <figure class="image-wrap">
                                                <img src="<?= $img_icon; ?>"
                                                     alt="<?= $img_alt; ?>"
                                                />
                                            </figure>
                                        </div>
                                    <?php endwhile; ?>
                                </section>
                            <?php endif; ?>

                            <section class="text-section-wrapper">
                                <div class="text-content-inner">
                                    <?php the_content(); ?>
                                </div>

                                <a href="<?php the_field('button_url'); ?>"
                                   class="cta <?= $btn_colors ?>-cta"
                                >
                                    <?php the_field('button_text'); ?>
                                </a>

                                <a href="<?= get_permalink(get_page_by_title('About Bolton')) ?>"
                                   class="cta <?= $btn_colors ?>-cta"
                                >
                                    About Bolton
                                </a>

                                <a href="<?= get_permalink(get_page_by_title('Why invest')) ?>"
                                   class="cta <?= $btn_colors ?>-cta"
                                >
                                    Why invest?
                                </a>
                            </section>
                        </div>

<!--                        <img src="--><?//= THEME_PATH; ?><!--/gfx/bolton-council-logo.svg"-->
<!--                             alt="Bolton council logo"-->
<!--                             class="council-logo-area"-->
<!--                        />-->
                    </div>
                </article>
            <?php endwhile;

            wp_reset_postdata();
        else: ?>
            <p><?php _e('Sorry, no intervention areas matched your criteria.'); ?></p>
        <?php endif; ?>
    </section>
</main>
<?php get_footer();
