<!doctype html>
<html class="no-js" <?php language_attributes('html'); ?>> <!--<![endif]-->

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <link rel="shortcut icon"
          href="<?php echo get_stylesheet_directory_uri(); ?>/gfx/favicon.ico"
          type="image/x-icon">
    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/gfx/favicon.ico"
          type="image/x-icon">

    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <?php
    if ((is_search())) : ?>
        <meta name="robots" content="noindex, nofollow"/>
    <?php endif; ?>

    <title>
        <?php
        if (is_archive()) {
            echo ucfirst(trim(wp_title('', false))).' - ';
        } else if (!(is_404()) && (is_single()) || (is_page() || (is_home()))) {
            $title = wp_title('', false);

            if (!empty($title)) {
                echo $title.' - ';
            }
        } else if (is_404()) {
            echo 'Page can not be found!';
        }

        if (is_front_page()) {

            bloginfo('description');
        } else {
            echo bloginfo('name');
        }


        global $paged;

        if ($paged > 1) {
            echo ' - page '.$paged;
        }
        ?>
    </title>


    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121202514-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-121202514-1');
</script>


    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php wp_head(); ?>
</head>
<body <?php body_class(['is-loading sliders']); ?>>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a
        href="https://browsehappy.com/">upgrade your browser</a> to improve your
    experience and security.</p>
<![endif]-->

<!-- Add your site or application content here -->

<?php get_template_part('templates/loader', 'spinner') ?>


<!-- Navigation start -->
<div class="toogle-wrapper slider-switcher">
    <div class="nav-switcher">
        <span class="sandwich"></span>
        <span class="sandwich"></span>
        <span class="sandwich"></span>
    </div>
</div>
<?php get_template_part('nav'); ?>