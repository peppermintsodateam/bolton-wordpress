<?php
get_header('pages');
?>

    <main class="archive-main">
        <header class="main-back-header">
            <div class="page-content-header">
                <h1 class="page-main-header">
                    Sponsors
                </h1>
            </div>
        </header>

        <div class="sponsor-slider owl-carousel">
            <?php get_template_part('loops/post', 'sponsor'); ?>
        </div>
    </main>
<?php
get_footer('pages');