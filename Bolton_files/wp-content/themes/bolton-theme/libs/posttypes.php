<?php
add_action('init', 'bolton_init_posttypes');

function bolton_init_posttypes()
{
    $labels = [
        'name' => __('Intervention areas'),
        'singular_name' => __('Intervention area'),
        'add_new' => __('Add new intervention area'),
        'add_new_item' => __('Add new intervention area'),
        'edit_item' => __('Edit intervention area'),
        'new_item' => __('New intervention area'),
        'view_item' => __('View intervention area'),
        'search_items' => __('Search intervention area'),
        'not_found' => __('No intervention area found'),
        'not_found_in_trash' => __('No intervention area found in trash'),
    ];

    $args = [
        'labels' => $labels,
        'has_archive' => true,
        'public' => true,
        'hierarchical' => false,
        'menu_position' => 5,
        'supports' => [
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'page-attributes',
        ],
    ];

    register_post_type('intervention_areas', $args);

/*
    $labels = [
        'name' => __('Crompton place'),
        'singular_name' => __('crompton place'),
        'add_new' => __('Add new crompton place'),
        'add_new_item' => __('Add new crompton place'),
        'edit_item' => __('Edit crompton place'),
        'new_item' => __('New crompton place'),
        'view_item' => __('View crompton place'),
        'search_items' => __('Search crompton place'),
        'not_found' => __('No crompton place found'),
        'not_found_in_trash' => __('No crompton place found in trash'),
    ];

    $args = [
        'labels' => $labels,
        'has_archive' => true,
        'public' => true,
        'hierarchical' => false,
        'menu_position' => 5,
        'supports' => [
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'page-attributes',
        ],
    ];

    register_post_type('crompton_place', $args);

    $labels = [
        'name' => __('Cheadle square'),
        'singular_name' => __('cheadle square'),
        'add_new' => __('Add new cheadle square'),
        'add_new_item' => __('Add new cheadle square'),
        'edit_item' => __('Edit cheadle square'),
        'new_item' => __('New cheadle square'),
        'view_item' => __('View cheadle square'),
        'search_items' => __('Search cheadle square'),
        'not_found' => __('No cheadle square found'),
        'not_found_in_trash' => __('No cheadle square found in trash'),
    ];

    $args = [
        'labels' => $labels,
        'has_archive' => true,
        'public' => true,
        'hierarchical' => false,
        'menu_position' => 5,
        'supports' => [
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'page-attributes',
        ],
    ];

    register_post_type('cheadle_square', $args);

    $labels = [
        'name' => __('Trinity quarter'),
        'singular_name' => __('trinity quarter'),
        'add_new' => __('Add new trinity quarter'),
        'add_new_item' => __('Add new trinity quarter'),
        'edit_item' => __('Edit trinity quarter'),
        'new_item' => __('New trinity quarter'),
        'view_item' => __('View trinity quarter'),
        'search_items' => __('Search trinity quarter'),
        'not_found' => __('No trinity quarter found'),
        'not_found_in_trash' => __('No trinity quarter found in trash'),
    ];

    $args = [
        'labels' => $labels,
        'has_archive' => true,
        'public' => true,
        'hierarchical' => false,
        'menu_position' => 5,
        'supports' => [
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'page-attributes',
        ],
    ];

    register_post_type('trinity_quarter', $args);



    $labels = [
        'name' => __('Croal valley'),
        'singular_name' => __('Croal valley'),
        'add_new' => __('Add new croal valley'),
        'add_new_item' => __('Add new croal valley'),
        'edit_item' => __('Edit croal valley'),
        'new_item' => __('New croal valley'),
        'view_item' => __('View croal valley'),
        'search_items' => __('Search croal valley'),
        'not_found' => __('No croal valley found'),
        'not_found_in_trash' => __('No croal valley found in trash'),
    ];

    $args = [
        'labels' => $labels,
        'has_archive' => true,
        'public' => true,
        'hierarchical' => false,
        'menu_position' => 5,
        'supports' => [
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'page-attributes',
        ],
    ];

    register_post_type('croal_valley', $args);

    $labels = [
        'name' => __('Church wharf'),
        'singular_name' => __('church wharf'),
        'add_new' => __('Add new church wharf'),
        'add_new_item' => __('Add new church wharf'),
        'edit_item' => __('Edit church wharf'),
        'new_item' => __('New church wharf'),
        'view_item' => __('View church wharf'),
        'search_items' => __('Search church wharf'),
        'not_found' => __('No church wharf found'),
        'not_found_in_trash' => __('No church wharf found in trash'),
    ];

    $args = [
        'labels' => $labels,
        'has_archive' => true,
        'public' => true,
        'hierarchical' => false,
        'menu_position' => 5,
        'supports' => [
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'page-attributes',
        ],
    ];

    register_post_type('church_wharf', $args);


    $labels = [
        'name' => __('Wider regen'),
        'singular_name' => __('wider regen'),
        'add_new' => __('Add new wider regen'),
        'add_new_item' => __('Add new wider regen'),
        'edit_item' => __('Edit wider regen'),
        'new_item' => __('New wider regen'),
        'view_item' => __('View wider regen'),
        'search_items' => __('Search wider regen'),
        'not_found' => __('No wider regen found'),
        'not_found_in_trash' => __('No wider regen found in trash'),
    ];

    $args = [
        'labels' => $labels,
        'has_archive' => true,
        'public' => true,
        'show_in_rest' => true,
        'hierarchical' => false,
        'menu_position' => 5,
        'supports' => [
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'page-attributes',
        ],
    ];

    register_post_type('wider_regen', $args);
*/

    $labels = [
        'name' => __('Sponsors and Partners'),
        'singular_name' => __('Sponsor/Partner'),
        'add_new' => __('Add new Sponsor/Partner'),
        'add_new_item' => __('Add new Sponsor/Partner'),
        'edit_item' => __('Edit Sponsor/Partner'),
        'new_item' => __('New Sponsor/Partner'),
        'view_item' => __('View Sponsor/Partner'),
        'search_items' => __('Search Sponsor/Partner'),
        'not_found' => __('No Sponsor/Partner found'),
        'not_found_in_trash' => __('No Sponsor/Partner found in trash'),
    ];

    $args = [
        'labels' => $labels,
        'has_archive' => true,
        'public' => true,
        'show_in_rest' => true,
        'hierarchical' => false,
        'menu_position' => 5,
        'supports' => [
            'title',
            'editor',
            'thumbnail',
        ],
        'rewrite' => ['slug' => 'sponsors-partners'],
    ];

    register_post_type('sponsor', $args);

    flush_rewrite_rules();
}


?>