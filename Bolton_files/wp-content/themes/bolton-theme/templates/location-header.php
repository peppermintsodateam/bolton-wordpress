<?php if (has_post_thumbnail()) {
    $bck_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
}

?>

<div style="background-image: url(<?= $bck_image; ?>)" id="parallax-header"
     class="parallax parallax-location"
>
    <header class="main-back-header">
        <div class="page-content-header">
            <h1 class="page-main-header">
                <?php the_title(); ?>
            </h1>
        </div>
    </header>
</div>