 <section id="news" class="development-section">
            <div class="development-block development-left">
                <div class="development-padder">
                    <h2 style="color: <?= get_field('primary_color'); ?>;">Council buys Crompton Place</h2>
                   
                    <p><?php the_field('news_section') ?></p>
                </div>
            </div>
           <div class="development-block development-left cover-image" style="background-image: url(<?= get_field('news_section_img'); ?>);"></div>
        </section>