<section id="development-intro" class="development-section">
            <div class="development-block development-left">
                <div class="development-padder">
                    <h1 style="color: <?= get_field('primary_color'); ?>;">Introduction to Area</h1>
                    <p><?= get_field('introduction'); ?></p>
                </div>
            </div>
            <div class="development-block development-right">
                <div class="development-padder">
                    <div class="video-container">
                        <iframe class="video" src="<?= get_field('introduction_video'); ?>" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </section>