<section id="development-site-vision" class="development-section">
            <div class="development-block development-left">
                <div class="development-padder">
                    <h1 style="color: <?= get_field('primary_color'); ?>;">Site Vision</h1>
                    <p><?= get_field('site_vision'); ?></p>
                    <div class="video-container">
                        <iframe class="video" src="<?= get_field('site_vision_video'); ?>" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            
            <div class="development-block">
                <div id="development-map-wrapper">
                    <div id="map"></div>
                     <script>var selected_map = '<?= get_field('site_vision_map'); ?>';</script>
                   
                </div>
            </div>

        </section>