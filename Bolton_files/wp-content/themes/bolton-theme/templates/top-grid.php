<section class="grid-column-row" id="section-top-grid">
    <?php if (have_rows('top_row_grid')) : ?>
        <?php while (have_rows('top_row_grid')) : the_row();
            $layout = get_sub_field('choose_grid_layout');
            $text = get_sub_field('text_content');
            $text_title = get_sub_field('text_title');
            $hero_image = get_sub_field('image_icon');

            if ($layout == 'text-column'): ?>
                <article class="text-cell grid-cell">
                    <div class="content-inner">
                        <h3 class="page-sub-main-header green-header"><?= $text_title; ?></h3>
                        <?= $text; ?>
                    </div>
                </article>
            <?php endif ?>

            <?php if ($layout == 'image-column'): ?>
                <article style="background-image:url(<?php echo esc_url($hero_image); ?>)"
                         class="bck-cell grid-cell yellow-gradient"
                ></article>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>
</section>