<?php
$postThumbnail = '';
if (get_the_post_thumbnail_url()) {
    $postThumbnail = get_the_post_thumbnail_url();
} ?>

<?php if (!empty($postThumbnail)) : ?>
<div <?= (!empty($postThumbnail) ? sprintf('style="background-image: url(%s)"', $postThumbnail) : '') ?>
    id="parallax-header"
    class="parallax parallax-contact"
>
<?php endif ?>
    <header class="main-back-header">
        <div class="page-content-header">
            <h1 class="page-main-header">
                <?php the_title() ?>
            </h1>
        </div>
    </header>
<?php if (!empty($postThumbnail)) : ?>
</div>
<?php endif;
