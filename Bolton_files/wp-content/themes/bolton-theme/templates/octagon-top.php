<section id="development-intro" class="octagon-section full">
	<article class="into-inner-octagon content-center">
		<h1 style="color: <?= get_field('primary_color'); ?>;">Introduction to Area</h1>
        <p><?= get_field('introduction'); ?></p>
	</article>
</section>