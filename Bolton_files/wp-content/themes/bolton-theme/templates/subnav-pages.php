<ul>
	<li class="active"><a href="#development-intro" class="anchor-scroll" data-skip-animation="true">Introduction to Area</a></li>
	<li><a href="#development-principles" class="anchor-scroll" data-skip-animation="true">Development Principles</a></li>
	<li><a href="#development-site-vision" class="anchor-scroll" data-skip-animation="true">Site Vision</a></li>
	<li><a href="#development-working-with-us" class="anchor-scroll" data-skip-animation="true">Working with us</a></li>
</ul>