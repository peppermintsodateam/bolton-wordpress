<header class="top-page-header">
    <div class="logo-wrapper">
        <a href="<?php the_field('main_logo_url', 'options'); ?>" class="logo-link"><img src="<?= THEME_PATH; ?>/gfx/bolton-council-logo.svg" alt="Bolton council logo" /></a>
    </div>
    <div class="toogle-wrapper blue-toggle developers-toggle">
        <p>Developments</p>
        <div class="nav-switcher">
            <span class="sandwich"></span>
            <span class="sandwich"></span>
            <span class="sandwich"></span>
        </div>
    </div>
    <?php wp_nav_menu([
        'menu'           => 'Top Menu',
        'container'      => false,
        'menu_id'        => 'header-long-menu',
    ]);
    ?>
    <ul class="social-nav-top">
        <li><a href="<?php the_field('enquire_btn_url', 'options'); ?>" id="btn-contact">Contact us</a></li>
        <?php if (have_rows('page_header_social_media', 'options')) :
            while (have_rows('page_header_social_media', 'options')) :
                the_row(); ?>
                <li><a target="_blank" href="<?= get_sub_field('social_url'); ?>"><i class="<?= get_sub_field('social_class'); ?>"></i></a></li>
            <?php endwhile;
        endif; ?>
    </ul>
    <div class="toogle-wrapper blue-toggle mobile-menu-toggle">
        <div class="nav-switcher">
            <span class="sandwich"></span>
            <span class="sandwich"></span>
            <span class="sandwich"></span>
        </div>
    </div>
</header>