<header class="rotator-header">
    <div class="header-panel">
        <a class="logo-header-top" href="<?= esc_url(home_url('/')); ?>">
            <img src="<?= THEME_PATH; ?>/gfx/logo-small-bolton.svg" alt="">
        </a>
    </div>

    <a href="<?php the_field('back_btn_url', 'options') ?>"
       class="enquiry-btn header-item icon-btn"
    >
        <?php the_field('back_btn_text', 'options'); ?>
    </a>

    <div class="social-header-wrapper">
        <ul class="social-nav-top">
            <?php if (have_rows('social_media_rotator', 'options')) :
                while (have_rows('social_media_rotator', 'options')) :
                    the_row();
                    $link = get_sub_field('social_url');
                    $class = get_sub_field('social_class'); ?>

                    <li>
                        <a target="_blank" href="<?= $link; ?>"><i
                                    class="<?= $class; ?>"></i></a>
                    </li>
                <?php endwhile;
            endif; ?>
        </ul>

        <a href="<?php the_field('enquire_link', 'options') ?>"
           class="enquiry-btn header-item"
        >
            <?php the_field('enquire_text', 'options') ?>
        </a>
    </div>
</header>