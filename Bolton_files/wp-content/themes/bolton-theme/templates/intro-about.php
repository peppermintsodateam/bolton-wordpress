<section id="section-intro" class="blue-intro global-section">
    <div class="content-center narrow-content">
        <h2 class="page-sub-main-header">
            <?php the_field('page_top_header_text'); ?>
        </h2>

        <?php if (have_posts()) :
            while (have_posts()) :
                the_post();
                the_content();
            endwhile;
        endif; ?>
    </div>
</section>