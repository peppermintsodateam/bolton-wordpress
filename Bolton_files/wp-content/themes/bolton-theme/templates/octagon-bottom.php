<section id="development-working-with-us" class="development-section">
            
            <div class="development-block development-right color-block" style="background-color: <?= get_field('primary_color'); ?>;">
                <div class="development-padder">
                    <h2 style="color:#fff;">Site vision</h2>
                    <p><?= get_field('site_vision'); ?></p>
                    

                    <h2 style="color: #fff;">Working with us</h2>
                   <p><?= get_field('working_with_us'); ?></p>
                     <?php if (have_rows('working_with_us_author')) : ?>
                        <?php while (have_rows('working_with_us_author')) : the_row(); ?>
                            <p><i><?= get_sub_field('authors_name') ?><br /><?= get_sub_field('authors_business') ?></i></p>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="development-block development-left cover-image" style="background-image: url(<?= get_field('working_with_us_image'); ?>);"></div>
        </section>