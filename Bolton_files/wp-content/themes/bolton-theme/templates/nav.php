<nav class="main-navigation site-navigation" role="navigation">
    <div class="menu-close-button"></div>
    <div class="navigation-inner">
        <header class="nav-header">
            <h3 class="navigation-inner-header">
                <img src="<?= THEME_PATH; ?>/gfx/bolton-header-logo.svg"
                     alt="Bolton Council">
            </h3>
        </header>

        <div class="navigation-wrapper">
            <?php wp_nav_menu([
                'container'      => false,
                'menu_class'     => 'menu-left',
                'menu_id'        => 'menu-left',
                'theme_location' => 'left-menu',
            ]);
            ?>
        </div>

        <footer class="social-media-panel">
            <?php get_template_part('loops/social', 'panel'); ?>
        </footer>
    </div>
</nav>