<section class="quote-section quote-blue" id="section-quote">
    <div class="quote-inner-content">
        <div class="pos-center">
            <blockquote class="quote">
                <p>
                    <cite>
                        "One of the advantages Bolton has over other large towns and
                        cities is how well connected the public, private and charitable
                        sectors are. Partnership working and collaboration are over-used
                        buzzwords, but they seem very natural here in Bolton. That lends
                        itself well to building relationships and seeking out
                        opportunities."
                    </cite>
                </p>

                <footer class="footer-quote">
                    <span class="auothor-quote">Roddy Gauld, chief executive, Octagon Theatre</span>
                </footer>
            </blockquote>
        </div>
    </div>
</section>