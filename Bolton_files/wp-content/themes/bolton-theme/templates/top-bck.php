<?php if (has_post_thumbnail()) {
    $bck_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
}

$choose_shadow = get_field('background_header_options');

?>

<?php if ($choose_shadow == 'purple'): ?>

    <div style="background-image: url(<?php echo $bck_image; ?>);" id="parallax-header"
         class="parallax parallax-about">
        <header class="main-back-header">
            <div class="page-content-header">
                <h1 class="page-main-header">
                    <?php the_title(); ?>
                </h1>
            </div>
        </header>
    </div>

<?php endif; ?>


<?php if ($choose_shadow == 'green'): ?>

    <div style="background-image: url(<?php echo $bck_image; ?>);" id="parallax-header"
         class="parallax parallax-invest">
        <header class="main-back-header">
            <div class="page-content-header">
                <h1 class="page-main-header">
                    <?php the_title(); ?>
                </h1>
            </div>
        </header>
    </div>

<?php endif; ?>

<?php if ($choose_shadow == 'yellow'): ?>

    <div style="background-image: url(<?php echo $bck_image; ?>);" id="parallax-header"
         class="parallax parallax-commitment">
        <header class="main-back-header">
            <div class="page-content-header">
                <h1 class="page-main-header">
                    <?php the_title(); ?>
                </h1>
            </div>
        </header>
    </div>

<?php endif; ?>
