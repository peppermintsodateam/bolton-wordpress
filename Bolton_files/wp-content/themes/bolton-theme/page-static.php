<?php
/*
    Template name: Page static
*/

get_header('pages');
if (is_page(171)) {
?>
<nav class="sub-nav-developers" role="navigation">
    <h5><?php the_title(); ?></h5>
    <ul>
        <li><a href="#section-top-grid" class="anchor-scroll" data-skip-animation="true">Economy and Employment</a></li>
        <li><a href="#section-1" class="anchor-scroll" data-skip-animation="true">Education</a></li>
        <li><a href="#section-2" class="anchor-scroll" data-skip-animation="true">Culture</a></li>
    </ul>
</nav>
<label for="mobile-sub-nav-developers" class="screen-reader-only">Select Section:</label>
<select id="mobile-sub-nav-developers">
    <option value="#section-top-grid">Economy and Employment</option>
    <option value="#section-1">Education</option>
    <option value="#section-2">Culture</option>
</select>
<?php
}
get_template_part('templates/top', 'bck');

if (is_page(171)) {
    get_template_part('templates/intro', 'about');
    get_template_part('templates/top', 'grid');
    get_template_part('templates/quote', 'section');
} elseif (is_page(175)) {
    get_template_part('templates/intro', 'invest');
} elseif (is_page(177)) {
    get_template_part('templates/intro', 'project');
}

$section_index = 1;
if (get_field('reverse_column')): ?>
    <?php if (have_rows('sections_grid')) : ?>
        <?php while (have_rows('sections_grid')) :
            the_row(); ?>
            <section class="grid-column-row reverse-order" id="section-<?= $section_index++ ?>">
                <?php if (have_rows('new_article')) : ?>
                    <?php while (have_rows('new_article')) : the_row();
                        $grid_layout = get_sub_field('choose_grid');
                        $article_text = get_sub_field('text_content');
                        $article_title = get_sub_field('text_title');
                        $article_image = get_sub_field('image_icon');

                        if ($grid_layout == 'text-column'): ?>
                            <article class="text-cell grid-cell">
                                <div class="content-inner">
                                    <h3 class="page-sub-main-header green-header"><?= $article_title; ?></h3>
                                    <?= wpautop($article_text); ?>
                                </div>
                            </article>
                        <?php endif ?>

                        <?php if ($grid_layout == 'image-column'): ?>
                            <article style="background-image:url(<?php echo esc_url($article_image); ?>)"
                                     class="bck-cell grid-cell yellow-gradient"
                            ></article>
                        <?php endif;
                    endwhile;
                endif; ?>
            </section>
        <?php endwhile; ?>
    <?php endif; ?>
<?php else: ?>
    <?php if (have_rows('sections_grid')) : ?>
        <?php while (have_rows('sections_grid')) :
            the_row(); ?>
            <section class="grid-column-row" id="section-<?= $section_index++ ?>">
                <?php if (have_rows('new_article')) : ?>
                    <?php while (have_rows('new_article')) :
                        the_row();
                        $grid_layout = get_sub_field('choose_grid');
                        $article_text = get_sub_field('text_content');
                        $article_title = get_sub_field('text_title');
                        $article_image = get_sub_field('image_icon');

                        if ($grid_layout == 'text-column'): ?>
                            <article class="text-cell grid-cell">
                                <div class="content-inner">
                                    <h3 class="page-sub-main-header green-header"><?= $article_title; ?></h3>
                                    <?= wpautop($article_text); ?>
                                </div>
                            </article>
                        <?php endif;

                        if ($grid_layout == 'image-column'): ?>
                            <article style="background-image:url(<?php echo esc_url($article_image); ?>)"
                                     class="bck-cell grid-cell yellow-gradient"
                            ></article>
                        <?php endif;
                    endwhile;
                endif; ?>
            </section>
        <?php endwhile;
    endif;
endif;

get_footer('pages');
