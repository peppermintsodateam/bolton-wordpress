<?php
/*
    Template Name: Page rotator
*/
?>

<?php
get_header('pages')
?>
<!-- <main> -->
<section id="main-slides-container">
    <?php
    //varisbles
    $rotator_name = get_field('class_name');
    ?>

    <div id="main-rotator-wrapper" class="<?= $rotator_name; ?>">
        <div class="mask-slides"></div>

        <div class="slides-container">
            <?php get_template_part('loops/post', 'area') ?>
        </div>
    </div>
</section>
<!-- </main> -->

<?php get_footer(); ?>

