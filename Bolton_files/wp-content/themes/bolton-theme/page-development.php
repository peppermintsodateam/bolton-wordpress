<?php
/*
    Template Name: Development Page
*/
?>

<?php
get_header('pages');

$args = [
    'post_type' => 'page',
    'pagename' => $pagename,
    'post_status' => 'publish',
    'posts_per_page' => -1,
];

$page_loop = new WP_Query($args);

if ($page_loop->have_posts()) :
    while ($page_loop->have_posts()) :
        $page_loop->the_post(); ?>
        <nav class="sub-nav-developers" role="navigation">
            <h5><?php the_title(); ?></h5>

            

            <?php if(is_page(462)) {
                get_template_part("templates/subnav","octagon");
                }else {
                    get_template_part("templates/subnav","pages");
                    } ?>


        </nav>
        <label for="mobile-sub-nav-developers" class="screen-reader-only">Select Section:</label>
        <select id="mobile-sub-nav-developers">
            <option value="#development-intro">Introduction to Area</option>
            <option value="#development-principles">Development Principles</option>
            <option value="#development-site-vision">Site Vision</option>
            <option value="#development-working-with-us">Working with us</option>
        </select>
        <section id="development-top-logo" class="top-logo-wrapper <?php the_slug(); ?>" style="background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.3)), <?= get_field('primary_color'); ?> url(<?= get_field('header_pattern_image'); ?>) repeat scroll 0 0;">
            <div id="map-wrapper" style="background: transparent url(<?= get_field('header_map_image'); ?>) no-repeat scroll 50% 50%;">
                <div class="vertical-center">
                    <?php the_post_thumbnail('medium', array('alt' => get_post_type_object($pagename)->labels->singular_name)); ?>
                    <?php the_content(); ?>
                </div>
            </div>
        </section>

        <?php if(is_page(462)) {
            get_template_part("templates/octagon",'top');
            } else {
                get_template_part("templates/intro",'module');
            }

        ?>

      

        <section id="development-principles" class="development-section">
            <div class="development-block development-left cover-image" style="background-image: url(<?= get_field('development_principles_image'); ?>);"></div>
            <div class="development-block development-right color-block" style="background-color: <?= get_field('primary_color'); ?>;">
                <div class="development-padder">
                    <h2 style="color:#fff;">Development Principles</h2>
                    <table>
                    <?php if (have_rows('development_principles')) : ?>
                        <?php while (have_rows('development_principles')) : the_row(); ?>
                            <tr>
                                <td>
                                    <img src="<?= get_sub_field('icon')['url']; ?>" alt="<?= get_sub_field('icon_alt_text') ?>">
                                </td>
                                <td>
                                    <p><?= get_sub_field('principle') ?></p>
                                </td>
                            </tr>
                        <?php endwhile; ?>
                    <?php endif; ?>
                    </table>
                </div>
            </div>
        </section>


        <?php if(!is_page(462)) {
            get_template_part("templates/module","map");
        }
        ?>

        <?php if(is_page(462)) {
                get_template_part("templates/octagon","bottom");
            }else {
                get_template_part("templates/module","working");
            }
        ?>

        <?php
             if(is_page(339)) {
                get_template_part("templates/module","news"); }
         ?>
        

       


    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>
<?php else: ?>
    <p><?php _e('Sorry, no intervention areas matched your criteria.'); ?></p>
<?php endif; ?>
<?php get_footer('pages'); ?>