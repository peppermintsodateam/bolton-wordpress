<?php
/*
    Template Name: Wider Regeneration Page
*/
?>

<?php
get_header('pages');

$args = [
    'post_type' => 'page',
    'pagename' => $pagename,
    'post_status' => 'publish',
    'posts_per_page' => -1,
];

$page_loop = new WP_Query($args);

if ($page_loop->have_posts()) :
    while ($page_loop->have_posts()) :
        $page_loop->the_post(); ?>
        <nav class="sub-nav-developers" role="navigation">
            <h5><?php the_title(); ?></h5>
            <ul>
                <li class="active"><a href="#development-section-1" class="anchor-scroll" data-skip-animation="true"><?= get_field('section_1_title'); ?></a></li>
                <li><a href="#development-section-2" class="anchor-scroll" data-skip-animation="true"><?= get_field('section_2_title'); ?></a></li>
                <li><a href="#development-section-3" class="anchor-scroll" data-skip-animation="true"><?= get_field('section_3_title'); ?></a></li>
                <li><a href="#development-section-4" class="anchor-scroll" data-skip-animation="true"><?= get_field('section_4_title'); ?></a></li>
            </ul>
        </nav>
        <label for="mobile-sub-nav-developers" class="screen-reader-only">Select Section:</label>
        <select id="mobile-sub-nav-developers">
            <option value="#development-section-1"><?= get_field('section_1_title'); ?></option>
            <option value="#development-section-2"><?= get_field('section_2_title'); ?></option>
            <option value="#development-section-3"><?= get_field('section_3_title'); ?></option>
            <option value="#development-section-4"><?= get_field('section_4_title'); ?></option>
        </select>
        <section id="development-top-logo" class="top-logo-wrapper <?php the_slug(); ?>" style="background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.3)), <?= get_field('primary_color'); ?> url(<?= get_field('header_pattern_image'); ?>) repeat scroll 0 0;">
            <div id="map-wrapper" style="background: transparent url(<?= get_field('header_map_image'); ?>) no-repeat scroll 50% 50%;">
                <div class="vertical-center">
                    <?php the_post_thumbnail('medium', array('alt' => get_post_type_object($pagename)->labels->singular_name)); ?>
                    <?php the_content(); ?>
                </div>
            </div>
        </section>
        <section id="development-section-1" class="development-section">
            <div class="development-block development-left color-block" style="background-color: <?= get_field('primary_color'); ?>;">
                <div class="development-padder">
                    <h1><?= get_field('section_1_title'); ?></h1>
                    <p><?= get_field('section_1_text'); ?></p>
                </div>
            </div>
            <div class="development-block development-right">
                <div class="development-padder">
                    <div class="video-container">
                        <iframe class="video" src="<?= get_field('section_1_video'); ?>" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </section>
        <section id="development-section-2" class="development-section">
            <div class="development-block development-left">
                <div class="development-padder">
                    <div class="video-container">
                        <iframe class="video" src="<?= get_field('section_2_video'); ?>" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div class="development-block development-right color-block" style="background-color: <?= get_field('primary_color'); ?>;">
                <div class="development-padder">
                    <h1><?= get_field('section_2_title'); ?></h1>
                    <p><?= get_field('section_2_text'); ?></p>
                </div>
            </div>
            </div>
        </section>
        <section id="development-section-3" class="development-section">
            <div class="development-block development-left color-block" style="background-color: <?= get_field('primary_color'); ?>;">
                <div class="development-padder">
                    <h1><?= get_field('section_3_title'); ?></h1>
                    <p><?= get_field('section_3_text'); ?></p>
                </div>
            </div>
            <div class="development-block development-right cover-image" style="background-image: url(<?= get_field('section_3_image'); ?>);"></div>
        </section>
        <section id="development-section-4" class="development-section">
            <div class="development-block development-left cover-image" style="background-image: url(<?= get_field('section_4_image'); ?>);"></div>
            <div class="development-block development-right color-block" style="background-color: <?= get_field('primary_color'); ?>;">
                <div class="development-padder">
                    <h1><?= get_field('section_4_title'); ?></h1>
                    <p><?= get_field('section_4_text'); ?></p>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>
<?php else: ?>
    <p><?php _e('Sorry, no intervention areas matched your criteria.'); ?></p>
<?php endif; ?>
<?php get_footer('pages'); ?>